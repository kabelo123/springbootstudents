package com.yourname.Service;

import com.yourname.Dao.StudentDao;
import com.yourname.Entity.Student;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService{

    private StudentDao studentDao;

    public StudentServiceImpl(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public List<Student> getAllStudents() {
        return studentDao.findAll();
    }

    public Student getStudentByID(int id){
        return studentDao.getOne(id);
    }

    public boolean removeStudentById(int id) {
         Student student  = studentDao.getOne(id);
         if (student == null){
             studentDao.delete(student);
             return true;
         }
         return false;
    }
    public void updateStudent(Student student){
        studentDao.save(student);
    }

    public void insertStudent(Student student) {
        studentDao.save(student);
    }

    @Override
    public int getAge(String idNumber) {
        return 0;
    }
}
