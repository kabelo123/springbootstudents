package com.yourname.Service;

import com.yourname.Entity.Course;

import java.util.List;

public interface CourseService {

    List<Course> getAllCourses();

    Course getCourseByID(int id);

    boolean removeCourseById(int id);

    void updateCourse(Course course);

    void insertCourse(Course course);


}
