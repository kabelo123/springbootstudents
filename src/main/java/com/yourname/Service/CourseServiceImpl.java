package com.yourname.Service;

import com.yourname.Dao.CourseDao;
import com.yourname.Entity.Course;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {

    private CourseDao courseDao;

    public CourseServiceImpl(CourseDao courseDao) {
        this.courseDao = courseDao;
    }

    @Override
    public List<Course> getAllCourses() {
        return courseDao.findAll();
    }

    public Course getCourseByID(int id) {
        return courseDao.getOne(id);
    }

    public boolean removeCourseById(int id) {
        Course course = courseDao.getOne(id);
        if(courseDao == null){
            courseDao.delete(course);
            return true;
        }
        return false;
    }

    public void updateCourse(Course course) {
        courseDao.save(course);
    }

    public void insertCourse(Course course) {
        courseDao.save(course);
    }
}
