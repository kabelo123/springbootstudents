package com.yourname.Service;

import com.yourname.Entity.Student;

import java.util.List;

public interface StudentService {

    List<Student> getAllStudents();

    Student getStudentByID(int id);

    boolean removeStudentById(int id);

    void updateStudent(Student student);

    void insertStudent(Student student);

    int getAge(String idNumber);
}
