package com.yourname.Controller;

import com.yourname.Entity.Course;
import com.yourname.Service.CourseService;
import com.yourname.Service.CourseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/Courses")
public class CourseController {

    @Autowired
    private CourseServiceImpl courseServiceImpl;

    @GetMapping
    public Collection<Course> getAllCourses(){
        return courseServiceImpl.getAllCourses();
    }

    @GetMapping("/{id}")
    public Course getCourseByID(@PathVariable("id") int id){
        return courseServiceImpl.getCourseByID(id);
    }

    @DeleteMapping(value = "/{id}")
    public boolean deleteCourseByid(@PathVariable("id") int id){
        return courseServiceImpl.removeCourseById(id);
    }

    @PutMapping
    public Course updateCourse(@RequestBody Course course){
        courseServiceImpl.updateCourse(course);
        return course;
    }

    @PostMapping
    public Course insertCourse(@RequestBody Course course){
        courseServiceImpl.insertCourse(course);
        return course;
    }
}
