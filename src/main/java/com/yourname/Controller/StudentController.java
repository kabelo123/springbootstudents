package com.yourname.Controller;

import com.yourname.Entity.Student;
import com.yourname.Service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentServiceImpl studentServiceImpl;

    @GetMapping
    public Collection<Student> getAllStudents(){
        return studentServiceImpl.getAllStudents();
    }

    @GetMapping("/{id}")
   public Student getStudentByID(@PathVariable("id") int id){
       return studentServiceImpl.getStudentByID(id);
   }

    @DeleteMapping(value = "/{id}")
    public boolean deleteStudentByid(@PathVariable("id") int id){
        return studentServiceImpl.removeStudentById(id);
    }

    @PutMapping
    public Student updateStudent(@RequestBody Student student){
        studentServiceImpl.updateStudent(student);
        return student;
    }

    @PostMapping
    public Student insertStudent(@RequestBody Student student){
        studentServiceImpl.insertStudent(student);
        return student;
    }

}
