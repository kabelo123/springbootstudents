package com.yourname.Entity;

import org.hibernate.annotations.ValueGenerationType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "Course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @Size(max = 100)
    @Column(name = "Course Name")
    private  String courseName;
    @Size(max = 100)
    @Column(name = "Course Description")
    private  String courseDescription;
    @Column(name = "Start Date")
    private Date startDate;

    public Course() {

    }

    public Course(int id, @NotNull @Size(max = 100) String courseName, @Size(max = 100) String courseDescription, Date startDate) {
        this.id = id;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @OneToMany(cascade =CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "course");
}
